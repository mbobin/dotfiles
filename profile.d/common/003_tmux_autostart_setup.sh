if (command -v tmux>/dev/null) && [[ ! $TERM =~ screen ]] && [ -z $TMUX ] && [ "$TMUX_ENABLED" == "yes" ]; then
  export TMUX_DIST_CONFIG=$(uname -s | tr '[:upper:]' '[:lower:]')

  /usr/bin/env tmux -u new -s 0 -n 'myWindow' -d

  /usr/bin/env tmux send-keys -t 0:myWindow.0 'pmset -g thermlog' C-j

  /usr/bin/env tmux split-window -v
  /usr/bin/env tmux split-window -v
  /usr/bin/env tmux split-window -v -t 1

  /usr/bin/env tmux split-window -h -t 0

  /usr/bin/env tmux send-keys -t 1 'ping gitlab.org' C-j
  /usr/bin/env tmux send-keys -t 2 'htop' C-j
  /usr/bin/env tmux send-keys -t 3 'ctop' C-j

  /usr/bin/env tmux select-window -t 0:myWindow

  /usr/bin/env tmux -u attach -t 0:myWindow.4
fi
