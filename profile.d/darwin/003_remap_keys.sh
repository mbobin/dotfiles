function remap_keys {
  # swaps §/± with `/~
  # remaps caps lock to escape
  # https://apple.stackexchange.com/a/349440
  # https://developer.apple.com/library/archive/technotes/tn2450/_index.html

  hidutil property --set '{
    "UserKeyMapping":[
      {"HIDKeyboardModifierMappingSrc":0x700000064,"HIDKeyboardModifierMappingDst":0x700000035},
      {"HIDKeyboardModifierMappingSrc":0x700000035,"HIDKeyboardModifierMappingDst":0x700000064},
      {"HIDKeyboardModifierMappingSrc":0x700000039,"HIDKeyboardModifierMappingDst":0x700000029}
    ]
  }'
}
