for i in ~/Workspace/dotfiles/profile.d/common/*.sh; do
  if [ -r $i ]; then
    . $i
  fi
done
unset i

dist_name=$(uname -s | tr '[:upper:]' '[:lower:]')
if [ -d ~/Workspace/dotfiles/profile.d/${dist_name} ]; then
  for i in ~/Workspace/dotfiles/profile.d/${dist_name}/*.sh; do
    if [ -r $i ]; then
      . $i
    fi
  done
  unset i
fi
unset dist_name
