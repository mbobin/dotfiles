require "pathname"
require "fileutils"
require "rubygems/user_interaction"

module Helper
  extend Gem::UserInteraction
  extend FileUtils

  YES_VALUES = ['', 'y', 'yes']

  module_function

  def yes?(question)
    YES_VALUES.include? ask("#{question} [Y/n]").to_s.downcase
  end

  def workdir_path
    path_src("~/Workspace")
  end

  def path_src(str)
    Pathname(str).expand_path
  end

  def create_workspace_dir
    mkdir_p(workdir_path)
  end

  def move_project
    project_path = path_src(".")
    return if project_path.parent == workdir_path

    mv(project_path, workdir_path)
  end

  def dots_dir
    Pathname(File.expand_path("../home/", __FILE__))
  end

  def copy_dotfiles
    Dir.each_child(dots_dir) do |filename|
      next unless Helper.yes?("copy #{filename} ?")

      cp dots_dir.join(filename), Pathname("~/#{filename}").expand_path
    end
  end

  def backup_dotfiles
    Dir.each_child(dots_dir) do |filename|
      next unless Helper.yes?("backup #{filename} ?")

      cp Pathname("~/#{filename}").expand_path, dots_dir.join(filename)
    end
  end
end

namespace :dotfiles do
  task default: %w[create_workspace_dir move_project add_dot_files]

  task :create_workspace_dir do
    Helper.create_workspace_dir
  end

  task :move_project do
    Helper.move_project
  end

  task :add_dot_files do
    Helper.copy_dotfiles
  end

  task :backup do
    puts self.method(:cp).source_location
    Helper.backup_dotfiles
  end
end

task default: :"dotfiles:default"
